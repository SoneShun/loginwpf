﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfLogin
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private LoginViewModel _LoginViewModel = new LoginViewModel();

        private StackPanel stackpanel()
        {
            StackPanel _ = new StackPanel();
            _.Orientation = Orientation.Horizontal;
            _.FlowDirection = FlowDirection.RightToLeft;
            _.HorizontalAlignment = HorizontalAlignment.Center;
            return _;
        }

        public MainWindow()
        {
            InitializeComponent();
            
            //Windowのサイズ
            Width = 500;
            Height = 500;

            // 一番外側のDockPanelを作成
            DockPanel dockPanel = new DockPanel();
            Content = dockPanel;

            // StackPanel を作成
            StackPanel stackPanel = stackpanel();
            
            // StackPanelにボタンを追加
            stackPanel.Children.Add(_LoginViewModel.Login_Button);
            
            // StackPanelをDockPanelに追加
            dockPanel.Children.Add(stackPanel);
            DockPanel.SetDock(stackPanel, Dock.Bottom);
            
            //Gridの作成
            Grid grid = new Grid();
            grid.HorizontalAlignment = HorizontalAlignment.Center;
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.ColumnDefinitions.Add(new ColumnDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            grid.RowDefinitions.Add(new RowDefinition());
            //GridにLabelを追加
            grid.AddChild(_LoginViewModel.ID_Label, 0, 0, 1, 1);
            grid.AddChild(_LoginViewModel.Passwd_Label, 1, 0, 1, 1);
            //GridにTextBoxを追加
            grid.AddChild(_LoginViewModel.ID_TextBox, 0, 1, 1, 2);
            grid.AddChild(_LoginViewModel.Passwd_PasswordBox, 1, 1, 1, 2);
            
            //GridをDockPanelに追加
            dockPanel.Children.Add(grid);
            DockPanel.SetDock(grid, Dock.Top);
            
            // Label 用の Style を追加
            Style labelStyle = new Style(typeof(Label));
            labelStyle.Setters.Add(new Setter(Label.ForegroundProperty, new SolidColorBrush(Color.FromRgb(68, 68, 68))));
            Resources.Add(typeof(Label), labelStyle);

            // TextBox 用の Style を追加
            Style textBoxStyle = new Style(typeof(TextBox));
            //textBoxStyle.Setters.Add(new Setter(TextBox.ForegroundProperty, new SolidColorBrush(Color.FromRgb(240, 240, 240))));
            //textBoxStyle.Setters.Add(new Setter(TextBox.BackgroundProperty, new SolidColorBrush(Color.FromRgb(68, 68, 68))));
            textBoxStyle.Setters.Add(new Setter(TextBox.MarginProperty, new Thickness(2)));
            textBoxStyle.Setters.Add(new Setter(TextBox.HeightProperty, (double)28));
            Resources.Add(typeof(TextBox), textBoxStyle);
            
            // Button 用の Style を追加
            Style buttonStyle = new Style(typeof(Button));
            buttonStyle.Setters.Add(new Setter(Button.ForegroundProperty, new SolidColorBrush(Color.FromRgb(240, 240, 240))));
            buttonStyle.Setters.Add(new Setter(Button.BackgroundProperty, new SolidColorBrush(Color.FromRgb(5, 147, 14 * 16 + 2))));
            buttonStyle.Setters.Add(new Setter(Button.HeightProperty, (double)28));
            buttonStyle.Setters.Add(new Setter(Button.MarginProperty, new Thickness(1)));
            Resources.Add(typeof(Button), buttonStyle);
        }
    }

    // 拡張メソッドを用意
    public static class GridExtensions
    {
        // 行と列を指定してコントロールを追加する
        public static Grid AddChild(this Grid grid, UIElement element, int rowIndex, int columnIndex, int rowSpan, int columnSpan)
        {
            if (rowSpan < 1)
            {
                throw new ArgumentOutOfRangeException("rowSpan");
            }
            if (columnSpan < 1)
            {
                throw new ArgumentOutOfRangeException("columnSpan");
            }
            // Grid に追加
            grid.Children.Add(element);

            // 位置を指定
            Grid.SetRow(element, rowIndex);
            Grid.SetColumn(element, columnIndex);

            // セルを結合
            Grid.SetRowSpan(element, rowSpan);
            Grid.SetColumnSpan(element, columnSpan);

            return grid;
        }
    }
}
