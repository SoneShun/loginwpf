﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Configuration;

namespace WpfLogin
{
    public class LoginViewModel
    {
        //public List<ID> ID { get; set; }
        //public List<Pass> Passwd { get; set; }

        public Label ID_Label { get; set; }
        public Label Passwd_Label { get; set; }
        public TextBox ID_TextBox { get; set; }
        public PasswordBox Passwd_PasswordBox { get; set; }
        public Button Login_Button { get; set; }

        public LoginViewModel()
        {
            this.ID_Label = Label("ID");
            ID_Label.Margin = new Thickness(0, 80, 0, 0);
            this.Passwd_Label = Label("Password");
            this.ID_TextBox = Textbox("ID");
            this.Passwd_PasswordBox = PasswdBox("Password");
            this.Login_Button = Button();
        }
        
        private Label Label(string content)
        {
            Label _ = new Label();
            _.Content = content;
            _.HorizontalAlignment = HorizontalAlignment.Center;
            _.FontWeight = FontWeights.Bold;
            return _;
        }

        private TextBox Textbox(string name)
        {
            TextBox _ = new TextBox();
            _.Width = 150;
            _.Height = 20;
            _.Name = name;
            return _;
        }

        private PasswordBox PasswdBox(string name)
        {
            PasswordBox _ = new PasswordBox();
            _.Width = 150;
            _.Height = 20;
            _.Name = name;
            _.Margin = new Thickness(0, 0, 0, 170);
            return _;
        }

        private Button Button()
        {
            Button _ = new Button();
            //ボタン作成
            _.Content = "Click";
            _.Name = "_button";
            _.Width = 70;
            _.Height = 30;
            _.HorizontalAlignment = HorizontalAlignment.Left;
            _.Margin = new Thickness(0, 0, 0, 50);

            //クリックイベント
            _.Click += new RoutedEventHandler(_buttonClick);

            return _;
        }

        private void _buttonClick(object sender, RoutedEventArgs e)
        {
            var _id = ID_TextBox.Text;
            var _pass = Passwd_PasswordBox.Password;

            using (var db = new DataClassesDataContext(ConfigurationManager.ConnectionStrings["WpfLogin.Properties.Settings.TIMECARDConnectionString"].ConnectionString))
            {
                try
                {
                    bool _any = db.M_ACCOUNT.Any(x => x.USERID == _id && x.PASS == _pass);
                    if (_any)
                    {
                        MessageBox.Show("Success");
                    }
                    else
                    {
                        MessageBox.Show("Failed");
                    }
                }
                catch (Exception ee) { }
            }
        }
    }
}
